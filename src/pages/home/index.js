import Link from 'umi/link';
import './index.css'
function BasicLayout(props) {
    return (
    <div>
    <Link to='/'>首页</Link>
      <Link to='/home/movie'>电影</Link>
      <Link to='/home/cinema'>影院</Link>
      <Link to='/home/show'>演出</Link>
      <Link to='/home/list'>榜单</Link>
      <Link to='/home/hotspot'>热点</Link>
      <Link to='/home/shop'>商城</Link>
      {props.children}
   </div>
  );
}

export default BasicLayout;