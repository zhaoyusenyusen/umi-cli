// ref: https://umijs.org/config/
export default {
  treeShaking: true,
  routes: [
    {
      path: '/',
      component: './home',
      routes: [
        {
          path: '/',
          component: './home/homepage/index.js',
        },
        {
          path: '/home/movie',
          component: './home/movie/index.js',
        },
        {
          path: '/home/cinema',
          component: './home/cinema/index.js',
        },
        {
          path: '/home/show',
          component: './home/show/index.js',
        },
        {
          path: '/home/list',
          component: './home/list/index.js',
        },
        {
          path: '/home/hotspot',
          component: './home/hotspot/index.js',
        },
        {
          path: '/home/shop',
          component: './home/shop/index.js',
        }
      ],
    },
  ],
  plugins: [
    // ref: https://umijs.org/plugin/umi-plugin-react.html
    [
      'umi-plugin-react',
      {
        antd: false,
        dva: false,
        dynamicImport: false,
        title: 'myapp',
        dll: false,
        routes: {
          exclude: [/components\//],
        },
      },
    ],
  ],
};
